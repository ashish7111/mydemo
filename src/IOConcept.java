import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IOConcept {

	public static void main(String[] args) throws IOException {
		
		FileReader fr = new FileReader("input.txt");
		FileWriter fw = new FileWriter("output.txt");
		
		int ch;
		
		while((ch = fr.read())!= -1) {
			System.out.println((ch));
			fw.write(ch);
		}
		
		
		fr.close();
		fw.close();


	}

}

package com.cerotid.bank.model;

import java.util.ArrayList;

public class BankTester {

	public static void main(String[] args) {

		Bank b1 = new Bank();
		b1.setBankName("American National Bank");
		Bank b2 = new Bank();
		b2.setBankName("Chase");
		

		Customer c1 = new Customer();
		Customer c2 = new Customer();
		Customer c3 = new Customer();
		Customer c4 = new Customer();

		c1.setCustomerName("Ashish Pandey");
		c2.setCustomerName("Pratiksha Giri");
		c3.setCustomerName("Laxmi ghimire");
		c4.setCustomerName("Mr Nobody");

		ArrayList<Customer> customerList = new ArrayList<>();
		customerList.add(c1);
		customerList.add(c2);
		
		ArrayList<Customer> customerList1 = new ArrayList<>();
		customerList1.add(c1);
		customerList1.add(c2);
		customerList1.add(c3);
		customerList1.add(c4);
		

		
		b1.setCustomers(customerList);
		b2.setCustomers(customerList1);


		c1.setAddress("Mamaghar");
		c2.setAddress("Papaghar");

		Account c1Acc1 = new Account();
		Account c1Acc2 = new Account();
		c1Acc1.setAccountType("Savings");
		c1Acc2.setAccountType("Checking");

		ArrayList<Account> c1AccList = new ArrayList<>();
		c1AccList.add(c1Acc1);
		c1AccList.add(c1Acc2);
		
		c1.setAccounts(c1AccList);
		c2.setAccounts(c1AccList);
		
		b1.printBankName(b1.getBankName());
		b2.printBankDetails();
		c1.printCustomerAccounts();
		c2.printCustomerDetails();
		c1Acc1.printAccountInfo();

	}

}

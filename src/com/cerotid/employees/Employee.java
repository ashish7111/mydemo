package com.cerotid.employees;

// Employee Class
public class Employee {

	private String name;
	private long ssn;
	private double salary;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSsn() {
		return ssn;
	}

	public void setSsn(long ssn) {
		this.ssn = ssn;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
//Prints all the information of individual employee obj
	void printEmployeeInfo() {

		System.out.printf(name + "\t" + ssn + "\t" + "%.2f", salary);
		System.out.println();

	}

//Prints the ssn and Name of an employee
	void printSocialSecurityandName() {
//		this.name = name;
//		this.ssn = ssn;
		System.out.println(name + "\t" + ssn);
		System.out.println();

	}

}

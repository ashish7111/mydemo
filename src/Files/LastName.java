package Files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class LastName {

	public static void main(String[] args) {
		try {
			FileWriter fw = new FileWriter("Customers.txt");
			PrintWriter pw = new PrintWriter(fw);
			
			pw.println("Pandey, Ashish");
			pw.println("Allen, James");
			pw.println("Oshis, Harry");
			pw.println("Giri, Pratiksha");
			pw.println("McDonald, Old");
			pw.println("Ellis, Portia");
			pw.println("Milner, Ngalo");
			pw.println("Ronaldo, Christiano");
			pw.println("Messi, Lionel");
			pw.println("Fool, April");
			
			pw.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}

}
